# borgbackup-docker

This project is a very rudimentary Docker-container just containing the tools `openssh-client` and `borgbackup`

## Installation
To use the Docker image, use: `registry.gitlab.com/maxmue/borgbackup-docker`

## Environment Variables
For a list of Environment Variables, check out [the Borgbackup documentation](https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables).

## Examples
In my project with [Borgbase](https://www.borgbase.com), I for example use:
* `BORG_PASSPHRASE`
* `BORG_REPO`: `ssh://XXXXX@XXXXX.repo.borgbase.com/./repo`
* `BORG_RSH`: `ssh -i /tmp/ssh-files/id_ed25519 -oUserKnownHostsFile=/tmp/ssh-files/known_hosts`
  - the files inside `ssh-files` are i.e. mounted via Kubernetes Secrets


# Update notice (for maintainers of the repo)
To update the Docker image, run a Container with the latest alpine image, connect to it and run:
```
apk update
apk search borgbackup
```
Then, create a new branch and update the versions in the `Dockerfile` and `.gitlab-ci.yml`.
Once the build and test succeeded, it can be merged into `main`.

