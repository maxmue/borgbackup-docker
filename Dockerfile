FROM alpine:3.18
RUN apk add --no-cache openssh-client py3-llfuse borgbackup=1.2.6-r0

ENTRYPOINT ["borg"]
CMD ["-V"]
